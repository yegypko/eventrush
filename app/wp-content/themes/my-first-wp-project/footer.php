<footer id="сontact" class="site-footer">

    <div class="site-footer__inner container container--narrow">

      <div class="group">
        <?php 
      $contactPosts = new WP_Query(array(
            'nopaging' => true,
            'post_type' => 'contact',
          ));

          while ($contactPosts->have_posts()) {
            $contactPosts->the_post('contact');?>
            <!-- в этот цикл добавляем верстку-->
                <div class="site-footer__col-one">
                  <h1 class="school-logo-text school-logo-text--alt-color"><a href="<?php echo site_url() ?>"><strong><?php the_title()?></strong></a></h1>
                  <p><?php the_field('tell');?></p>
                </div>

                <div class="site-footer__col-one">
                  
                    <h3 class="headline headline--small">Наш адресс</h3>
                    <nav class="nav-list">
                     <ul>
                        <li><?php the_field('adress');?></li>
                        <li><?php the_field('e-mail');?></li>
                      </ul> 
                    </nav>
                </div>

                <div class="site-footer__col-four">
                  <h3 class="headline headline--small"><?php the_field('connect-with-us');?></h3>
                  <nav>
                    <ul class="min-list social-icons-list group">
                      <li><a href="<?php the_field('facebook');?>" class="social-color-facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href="<?php the_field('twitter');?>" class="social-color-twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li><a href="<?php the_field('you_tube');?>" class="social-color-youtube"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                      <li><a href="<?php the_field('linkedin');?>" class="social-color-linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                      <li><a href="<?php the_field('instagram');?>" class="social-color-instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                    </ul>
                  </nav>
                </div>
                  

                  <?php } wp_reset_postdata();
    ?>

        
      </div>
    </div>
  </footer>
  
<?php wp_footer();?>
</body>
</html>