<div class="event-summary one-third">
  <p class="event-summary__date t-center" href="#">
    <span class="event-summary__month"><?php
      $eventDate = new DateTime(get_field('event_date'));
      echo $eventDate->format('M')
    ?></span>
    <span class="event-summary__day"><?php echo $eventDate->format('d') ?></span>  
  </p>
  <div class="event-summary__content">
    <h5 class="event-summary__title headline headline--tiny"><p><?php the_title(); ?></p></h5>
    <p class="event-content"><?php if (has_excerpt()) {
        echo get_the_excerpt();
      } else {
        echo get_the_content();
        } ?></p>
  </div>
</div>