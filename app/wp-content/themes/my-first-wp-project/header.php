<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head();?>
</head>
<body <?php body_class(); ?>>
<header class="site-header">
    <div class="container">
      <h1 class="school-logo-text float-left"><a href="<?php echo site_url()?>"><img src="<?php $LogoImage = get_field('logo', 'option'); echo $LogoImage['sizes']['ImageLogoHeder']?>" alt="logo"/></a></h1>
      
      <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
      <div class="site-header__menu group">
        <nav class="main-navigation">
         <!--  <?php 
          wp_nav_menu(array(
            'theme_location' => 'headerMenuLocation'

          ));

          ?> -->
          <ul>
            <li><a href="<?php echo site_url('#activity') ?>">Мероприятия</a></li>
            <li><a href="<?php echo site_url('#couch') ?>">Спикеры</a></li>
            <li><a href="<?php echo site_url('#subscribe') ?>">Подписаться</a></li>
            <li><a href="<?php echo site_url('#point') ?>">Место проведения</a></li>
            <li><a href="<?php echo site_url('#сontact') ?>">Контакты</a></li>
          </ul>
        </nav>
        <div class="site-header__util">

         <?php if(is_user_logged_in()) { ?>
            <!-- <a href="<?php echo esc_url(site_url('/my-notes')); ?>" class="btn btn--small btn--orange float-left push-right">My Notes</a> -->
            <a href="<?php echo wp_logout_url();  ?>" class="btn btn--small  btn--dark-orange float-left btn--with-photo">
            <span class="site-header__avatar"><?php echo get_avatar(get_current_user_id(), 60); ?></span>
            <span class="btn__text">Выйти</span>
            </a>
            <?php } else { ?>
              <a href="<?php echo wp_login_url(); ?>" class="btn btn--small btn--orange float-left push-right">Войти</a>
              <a href="<?php echo wp_registration_url(); ?>" class="btn btn--small  btn--dark-orange float-left">Регистрация</a>
             <?php } ?>
          
          
        </div>
      </div>
    </div>
  </header>