<?php get_header(); ?>

<!-- Slider-->
  <div class="hero-slider">
        <?php 
          $sliderPosts = new WP_Query(array(
            'nopaging' => true,
            'post_type' => 'slider' 
          ));
          while ($sliderPosts->have_posts()) {
            $sliderPosts->the_post('slider');?>
            <!-- FOR LOOP-->
            <div class="hero-slider__slide" style="background-image: url(<?php $sliderImage = get_field('slider_image'); echo $sliderImage['sizes']['sliderImage'];?>);">
              <div class="hero-slider__interior container">
                <div class="hero-slider__overlay">
                  <h2 class="headline headline--medium t-center"><?php the_title()?></h2>
                  <p class="t-center"><?php echo get_the_content() ?></p>
                  <p class="t-center no-margin"><a href="<?php the_permalink(); ?>" class="btn btn--orange">Далее</a></p>
                </div>
              </div>
            </div>
         <?php } wp_reset_postdata();?>
  </div> 
<!-- Slider Ower-->

<!-- Activity -->
  <div id="activity" class="container container--narrow page-section">
   <h2 class="headline headline--medium t-center">Мероприятия</h2>
      <div class="row group">
        <?php 
          $today = date('Ymd');
          $homepageEvents = new WP_Query(array(
            'posts_per_page' => -1,
            'post_type' => 'event',
            'meta_key' => 'event_date',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
              array(
                'key' => 'event_date',
                'compare' => '>=',
                'value' => $today,
                'type' => 'numeric'
              )
            )
          ));

          while($homepageEvents->have_posts()) {
            $homepageEvents->the_post(); 
              get_template_part('template-parts/content', 'event');
           }
        ?>                 
      </div>
     <?php echo '<hr class="section-break">';?>
    </div>
<!-- Activity Ower -->

<!-- Couch -->
<h2 class="headline headline--medium t-center">Наши спикеры</h2>
  <div id="couch" class="container container--narrow page-section">
      <?php 
        $couchPosts = new WP_Query(array(
              'nopaging' => true,
              // 'posts_per_page' => -1,
              'post_type' => 'couch',
            ));
            while ($couchPosts->have_posts()) {
              $couchPosts->the_post('couch');?>
              <!-- FOR LOOP-->
                <div class="one-third couch-portret t-center">
                  <?php the_post_thumbnail('professorPortrait'); ?>                                  
                </div>
                <h2 class="headline headline--large t-center "><?php the_title()?></h2>
                <div class="couch-content">
                  <p><?php the_field('content');?></p>
                  <p><a href="<?php the_field('social_link');?>" class="btn btn--small btn--blue btn-facebook no-margin t-center">facebook</a></p> 
                </div>
                  <?php echo '<hr class="section-break">';?>
            <?php } wp_reset_postdata();?>                       
  </div>
<!-- Couch Ower-->


<!-- Subscribe -->
<div id="subscribe" class="container container--subscribe t-center">
      <div class="generic-content">
        <div class="row group">
          <div class="one-third subscribe-content">
              <div>
               <?php if(is_user_logged_in()) { ?>
                  <a href="<?php echo wp_logout_url();  ?>" class="btn btn--large  btn--dark-orange btn--with-photo">
                  <span class="site-header__avatar"><?php echo get_avatar(get_current_user_id(), 60); ?></span>
                  <span class="btn__text">Отписаться от участия</span>
                  </a>
                  <?php } else { ?>
                    <a href="<?php echo wp_registration_url(); ?>" class="btn btn--large  btn--dark-orange">Подписаться на участие</a>
                   <?php } ?>
              </div>   
          </div>  
        </div>
      </div>
</div>
<!-- Subscribe Ower-->

<!-- Point -->
<div id="point" class="container container--narrow page-section">
      <?php 
            $campusPosts = new WP_Query(array(
                  'nopaging' => true,
                  // 'posts_per_page' => -1,
                  'post_type' => 'campus',
                ));

                while ($campusPosts->have_posts()) {
                  $campusPosts->the_post('campus');
                  $mapLocation = get_field('map_location');
                  ?>
                  <!-- FOR LOOP-->
                  
                    <div class="generic-content">
                      <h3 class="headline headline--medium t-center"><?php the_title(); ?></h3>
                      <!-- <?php the_content(); ?> -->
                    </div>
                    <div class="acf-map">
                      <div class="marker" data-lat="<?php echo $mapLocation['lat'] ?>" data-lng="<?php echo $mapLocation['lng']; ?>">
                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <?php echo $mapLocation['address']; ?>
                      </div>
                    </div>
                  <?php } wp_reset_postdata();?>
</div>

<!-- Point Ower-->

  <?php get_footer();?>