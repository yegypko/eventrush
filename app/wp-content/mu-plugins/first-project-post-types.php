<?php

function first_project_post_types() {

  // Slider Post Type
  register_post_type('slider', array(
    'has_archive' => true,
    'supports' => array('title', 'editor'),
    'public' => false,
    'show_ui' => true,
    'labels' => array(
      'name' => 'Слайдер',
      'add_new_item' => 'Add New Слайдер',
      'edit_item' => 'Долбавить слайдер',
      'all_items' => 'Все слайдеры',
      'singular_name' => 'Слайдер'
    ),
    'menu_icon' => 'dashicons-format-gallery'
  ));

  // Event Post Type
  register_post_type('event', array(
    'has_archive' => true,
    'supports' => array('title', 'editor'),
    'public' => false,
    'show_ui' => true,
    'labels' => array(
      'name' => 'События',
      'add_new_item' => 'Add New Событие',
      'edit_item' => 'Добавить событие',
      'all_items' => 'Все события',
      'singular_name' => 'Событие'
    ),
    'menu_icon' => 'dashicons-calendar'
  ));

  // Couch Post Type
  register_post_type('couch', array(
    'has_archive' => true,
    'supports' => array('title', 'thumbnail'),
    'public' => false,
    'show_ui' => true,
    'labels' => array(
      'name' => 'Тренеры',
      'add_new_item' => 'Add New Тренера',
      'edit_item' => 'Edit Тренера',
      'all_items' => 'Все Тренеры',
      'singular_name' => 'Тренер'
    ),
    'menu_icon' => 'dashicons-welcome-learn-more'
  ));

  // Campus Post type
  register_post_type('campus', array(
    'capability_type' => 'campus',
    'map_meta_cap' => true,
    'supports' => array('title','excerpt'),
    'rewrite' => array('slug' => 'campuses'),
    'has_archive' => true,
    'public' => false,
    'show_ui' => true,
    'labels' => array(
      'name' => 'Место',
      'add_new_item' => 'Добавить новое место',
      'edit_item' => 'Добавить место',
      'new_item'  => 'Новое место',
      'all_items' => 'Все места',
      'singular_name' => 'Место'
    ),
    'menu_icon' => 'dashicons-location-alt'
  ));
  
  // Contact Post Type
  register_post_type('contact', array(
    'has_archive' => true,
    'supports' => array('title'),
    'public' => false,
    'show_ui' => true,
    'labels' => array(
      'name' => 'Контакты',
      'add_new_item' => 'Добавить контакты',
      'edit_item' => 'Добавить контакт',
      'all_items' => 'Все контакты',
      'singular_name' => 'Контакт'
    ),
    'menu_icon' => 'dashicons-phone'
  ));


  // Program Post Type
  // register_post_type('program', array(
  //   'supports' => array('title'),
  //   'rewrite' => array('slug' => 'programs'),
  //   'has_archive' => true,
  //   'public' => true,
  //   'labels' => array(
  //     'name' => 'Program',
  //     'add_new_item' => 'Add New Program',
  //     'edit_item' => 'Edit Program',
  //     'all_items' => 'All Program',
  //     'singular_name' => 'Program'
  //   ),
  //   'menu_icon' => 'dashicons-awards'
  // ));
  // Professor Post Type
  // register_post_type('professor', array(
  //   'show_in_rest' => true,
  //   'supports' => array('title', 'editor', 'thumbnail'),
  //   'public' => true,
  //   'labels' => array(
  //     'name' => 'Professors',
  //     'add_new_item' => 'Add New Professor',
  //     'edit_item' => 'Edit Professor',
  //     'all_items' => 'All Professors',
  //     'singular_name' => 'Professor'
  //   ),
  //   'menu_icon' => 'dashicons-welcome-learn-more'
  // ));

  // Note Post Type
  // register_post_type('note', array(
  //   'capability_type' => 'note',
  //   'map_meta_cap' => true,
  //   'show_in_rest' => true,
  //   'supports' => array('title', 'editor'),
  //   'public' => false,
  //   'show_ui' => true,
  //   'labels' => array(
  //     'name' => 'Notes',
  //     'add_new_item' => 'Add New Note',
  //     'edit_item' => 'Edit Note',
  //     'all_items' => 'All Notes',
  //     'singular_name' => 'Note'
  //   ),
  //   'menu_icon' => 'dashicons-welcome-write-blog'
  // ));

  // Like Post Type
  // register_post_type('Like', array(
  //   'supports' => array('title'),
  //   'public' => false,
  //   'show_ui' => true,
  //   'labels' => array(
  //     'name' => 'Likes',
  //     'add_new_item' => 'Add New Like',
  //     'edit_item' => 'Edit Like',
  //     'all_items' => 'All Likes',
  //     'singular_name' => 'Like'
  //   ),
  //   'menu_icon' => 'dashicons-heart'
  // ));

}

add_action('init', 'first_project_post_types');